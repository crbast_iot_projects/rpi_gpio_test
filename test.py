from gpiozero import LED
from time import sleep
import requests

host = '...'  # Your host

led = LED(17)

while True:
    led.on()
    requests.post(f'{host}/ras', data={'text': 'LED ON'})
    sleep(1)
    led.off()
    requests.post(f'{host}/ras', data={'text': 'LED OFF'})
    sleep(1)
